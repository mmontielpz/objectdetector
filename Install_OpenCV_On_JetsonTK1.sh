### Remove all old opencv stuffs installed by JetPack (or OpenCV4Tegra)

sudo apt-get purge libopencv*

### I prefer using newer version of numpy (installed with pip), so
### I'd remove this python-numpy apt package as well

sudo apt-get purge python-numpy

### Remove other unused apt packages

sudo apt-get autoremove

### Upgrade all installed apt packages to the latest versions (optional)

sudo apt-get update
sudo apt-get dist-upgrade

### Update gcc apt package to the latest version (highly recommended)

sudo apt-get install --only-upgrade g++-5 cpp-5 gcc-5

### Install dependencies based on the Jetson Installing OpenCV Guide
sudo apt-get install build-essential make cmake cmake-curses-gui \
                       g++ libavformat-dev libavutil-dev \
                       libswscale-dev libv4l-dev libeigen3-dev \
                       libglew-dev libgtk2.0-dev
### Install dependencies for gstreamer stuffs
sudo apt-get install libdc1394-22-dev libxine2-dev \
                       libgstreamer1.0-dev \
                       libgstreamer-plugins-base1.0-dev
### Install additional dependencies according to the pyimageresearch
### article
 sudo apt-get install libjpeg8-dev libjpeg-turbo8-dev libtiff5-dev \
                       libjasper-dev libpng12-dev libavcodec-dev
 sudo apt-get install libxvidcore-dev libx264-dev libgtk-3-dev \
                       libatlas-base-dev gfortran
 sudo apt-get install libblas-dev liblapack-dev liblapacke-dev
### Install Qt5 dependencies
 sudo apt-get install qt5-default
### Install dependencies for python3
 sudo apt-get install python3-dev python3-pip python3-tk
 pip3 install numpy --user
 pip3 install matplotlib --user
### Modify matplotlibrc (line #41) as 'backend      : TkAgg'
 sudo gedit /usr/local/lib/python3.5/dist-packages/matplotlib/mpl-data/matplotlibrc
### Also install dependencies for python2
### Note that I install numpy with pip, so that I'd be using a newer
### version of numpy than the apt-get package
sudo apt-get install python-dev python-pip python-tk
pip2 install numpy --user
pip2 install matplotlib --user

### Modify matplotlibrc (line #41) as 'backend      : TkAgg'
sudo gedit /usr/local/lib/python2.7/dist-packages/matplotlib/mpl-data/matplotlibrc
