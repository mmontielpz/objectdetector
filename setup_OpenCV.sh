# Step 1: Update packages

sudo apt-get update -y
sudo apt-get upgrade -y

# Step 2: Install OS libraries

# Remove any previous installations of x264</h3>
sudo apt-get remove x264 libx264-dev -y
 
# We will Install dependencies now
 
sudo apt-get install build-essential checkinstall cmake pkg-config yasm -y
sudo apt-get install git gfortran -y
sudo apt-get install libjpeg8-dev libjasper-dev libpng12-dev -y
 
# If you are using Ubuntu 14.04
sudo apt-get install libtiff4-dev -y
 
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev -y
sudo apt-get install libxine2-dev libv4l-dev -y
sudo apt-get install libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev -y
sudo apt-get install qt5-default libgtk2.0-dev libtbb-dev -y
sudo apt-get install libatlas-base-dev -y
sudo apt-get install libfaac-dev libmp3lame-dev libtheora-dev -y
sudo apt-get install libvorbis-dev libxvidcore-dev -y
sudo apt-get install libopencore-amrnb-dev libopencore-amrwb-dev -y
sudo apt-get install x264 v4l-utils -y
 
# Optional dependencies
sudo apt-get install libprotobuf-dev protobuf-compiler -y
sudo apt-get install libgoogle-glog-dev libgflags-dev -y
sudo apt-get install libgphoto2-dev libeigen3-dev libhdf5-dev doxygen -y

# Step 3: Install Python libraries
sudo apt-get install python-dev python-pip python3-dev python3-pip -y
pip install numpy --user
pip3 install numpy --user 

sudo apt-get autoremove -y


