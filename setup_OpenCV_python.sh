sudo apt-get install python3-setuptools -y
sudo easy_install3 pip - y
sudo apt-get install python3-pip -y

# Install virtual environment
pip install virtualenv virtualenvwrapper --user
pip3 install virtualenv virtualenvwrapper --user

# Upgrade 
pip install --upgrade virtualenvwrapper --user
pip3 install --upgrade virtualenvwrapper --user

pip install --upgrade virtualenv --user
pip3 install --upgrade virtualenv --user

# Setup for the .bashrc
echo "# virtualenvwrapper setup" >> ~/.bashrc  
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python" >> ~/.bashrc  
echo "WORKON_HOME=$HOME/envs" >> ~/.bashrc
echo "PROJECT_HOME=$HOME/dev" >> ~/.bashrc
echo "source ~/.local/bin/virtualenvwrapper.sh" >> ~/.bashrc

source ~/.bashrc
  
############ For Python 2 ############
# create virtual environment
mkvirtualenv deeplearning-py2 -p python
workon deeplearning-py2
  
# now install python libraries within this virtual environment
pip install numpy scipy matplotlib scikit-image scikit-learn ipython --user
  
# quit virtual environment
deactivate
######################################
  
############ For Python 3 ############
# create virtual environment
mkvirtualenv deeplearning-py3 -p python3
workon deeplearning-py3
  
# now install python libraries within this virtual environment
pip install numpy scipy matplotlib scikit-image scikit-learn ipython --user
  
# quit virtual environment
deactivate
######################################

