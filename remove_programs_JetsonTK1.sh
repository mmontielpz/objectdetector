sudo apt-get remove firefox
sudo apt-get remove gnome-orca


### Accesories

sudo apt-get remove gnome-calculator
sudo apt-get remove gnome-contacts

### Games

sudo apt-get remove gnome-mahjong
sudo apt-get remove gnome-mines
sudo apt-get remove gnome-sudoku
sudo apt-get remove aisleriot


### Graphics


### Internet

sudo apt-get remove webbrowser-app
sudo apt-get remove empathy
sudo apt-get remove transmission-common

### Office

sudo apt-get remove unity-scope-gdrive

### Sound & Video

sudo apt-get remove brasero
sudo apt-get remove rhythmbox

### Autoremove

sudo apt-get autoremove

### Uncategorized

Amazon
Ubuntu One Music
