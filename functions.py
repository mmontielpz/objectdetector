# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')


filename = 'tjvideo1_processed.avi'
frames_per_second = 0.0
res ='720p'

# Set resolution for the vide VideoCapture
def change_res(cap, width, height):
	cap.set(3, width)
	cap.set(4, height)

# Standard Video Dimensions Sizes
STD_DIMENSIONS = {
	"480p" : (640, 480),
	"720p" : (1280, 720),
	"1080p" : (1920, 1080),
	"4k" : (3840, 2160)
}

# grab resolution dimensions and set video capture to it.
def get_dims(cap, res='1080p'):
	width , height = STD_DIMENSIONS["480p"]
	if res in STD_DIMENSIONS:
		width, height = STD_DIMENSIONS[res]
	# Change the current capture device
	# to the resoluting resolution
	change_res(cap, width, height)
	return width, height

# Video Encoding, might require additional installs
# Type of  Codes: http://www.fourcc.org/codes.php
VIDEO_TYPE = {
	'avi': cv2.VideoWriter_fourcc(*'XVID'),
	#'mp4' : cv2.VideoWriter_fourcc(*H264),
	'mp4': cv2.VideoWriter_fourcc(*'XVID'),
}

def get_video_type(filename):
	filename, ext = os.path.splitext(filename)
	if ext in VIDEO_TYPE:
		return VIDEO_TYPE[ext]
	return VIDEO_TYPE['avi']
