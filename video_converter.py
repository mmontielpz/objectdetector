#!/usr/bin/env python
# enconding: utf-8

import cv2

src_dir = "/home/mike/Videos/tjvideo1.mp4"
dst_dir = "/home/mike/Videos/tjvideo1.avi"
count = 0

video_cap = cv2.VideoCapture(src_dir)

fps = video_cap.get(cv2.CAP_PROP_FPS)
size = (int(video_cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(video_cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
#
video_writer = cv2.VideoWriter(dst_dir, cv2.VideoWriter_fourcc('M', 'J', 'P',
                                                               'G'), fps, size)

while (video_cap.isOpened()):
    success , frame = video_cap.read()

    if success==True:
        print('Read %d frame: ' % count, success)
        video_writer.write(frame)
        count +=1
    else:
        break

# When everything done, release the capture
video_writer.release()
cv2.destroyAllWindows()
